package com.example.untiled.dao;

import com.example.untiled.model.IsianBukuTamu;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// INI ADALAH MODEL BUKU TAMU

@Service
public class BukuTamu {
    List<IsianBukuTamu> bukuTamu;

    public BukuTamu() {
        bukuTamu = new ArrayList<>();

        IsianBukuTamu isian1 = new IsianBukuTamu();
        isian1.setNama("Dias");
        isian1.setPesan("Haiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        bukuTamu.add(isian1);
    }

    public void isiBukuTamu(IsianBukuTamu isian) {
        bukuTamu.add(isian);
    }

    public List<IsianBukuTamu> bacaBukuTamu() {
        return bukuTamu;
    }
}
