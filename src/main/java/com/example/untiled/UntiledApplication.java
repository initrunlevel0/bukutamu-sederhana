package com.example.untiled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UntiledApplication {

	public static void main(String[] args) {
		SpringApplication.run(UntiledApplication.class, args);
	}
}
