/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.untiled.controller;

import com.example.untiled.dao.BukuTamu;
import com.example.untiled.model.IsianBukuTamu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author initr
 */
@Controller
public class MainController {
	@Autowired
	private BukuTamu bukuTamu;

	@GetMapping("/")
	public String home(Model model) {
		List<IsianBukuTamu> isianBukuTamu = bukuTamu.bacaBukuTamu();
		model.addAttribute("bukuTamu", isianBukuTamu);
		return "home";
	}

	@PostMapping("/kirim")
	public String isi(HttpServletRequest request) {
		IsianBukuTamu isian = new IsianBukuTamu();
		isian.setNama(request.getParameter("nama"));
		isian.setPesan(request.getParameter("pesan"));
		bukuTamu.isiBukuTamu(isian);
		return "redirect:/";
	}

}
